/*
	Project : GHS algorithm for Minimal Spanning Tree in distributed environment.
	Author : Romain Agostinelli, Simon Corboz
	Date : January 2021
*/

package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net"
	"path/filepath"
	"sync"

	"gopkg.in/yaml.v2"
)

// PORT tcp port used to reach every node
var PORT = ":30000"

// yamlConfig, simple structure to retrieve the configuration of all nodes via yaml files.
type yamlConfig struct {
	ID         int         `yaml:"id"`
	Address    string      `yaml:"address"`
	Neighbours []Neighbour `yaml:"neighbours"`
}

// messageKind represents the enumeration of the 4 possibles message kinds.
type messageKind int

const (
	NEW_FRAGMENT messageKind = iota // coordination message sent by the root at the end of a stage
	CONNECT                         // Sent by the node incident to the MWOE to perform the union
	MERGE                           // Sent by the root to the node incident to the MWOE to perform the union
	TEST                            // Check the status of a basic edge
	ACCEPT                          // Positive response to TEST
	REJECT                          // Negative response to TEST
	REPORT                          // Report to the parent the MWOE of the appended subfragment
	TERMINATED                      // Sent by our watchdog to stop the message broker
	ACK                             // Broadcast with ACK
	DOTEST                          // Used to initiate tests once the coordination is done
)

// Message structure used for transfer data between nodes
// Weight data can be null in some cases depending on the message kind
// NeighbourTag data is the same
type Message struct {
	Kind         messageKind `json:"messageKind"`
	Source       *NodeImpl   `json:"node"`
	FragmentID   int         `json:"fragmentID"`
	Weight       int         `json:"weight,omitempty"`
	NeighbourTag int         `json:"neighbourTag,omitempty"` // used for O(1) access of neighbour in response message equals the index in the neighbour list

}

// Neighbour represent a node that is linked to the actual node. It wraps a NodeImpl (Node interface) with EdgeWeight.
type Neighbour struct {
	NodeImpl   `yaml:",inline"`
	EdgeWeight int `yaml:"edge_weight"`
}

func (n Neighbour) String() string {
	return fmt.Sprintf("%d", n.NodeImpl.ID())
}

// Node specifies what a Node is capable of.
type Node interface {
	// ID a Node must be capable of giving its id.
	ID() int
	// Addr a Node must be capable of giving its address (IP). This addr can be used without modification to send data.
	// The port must be contained.
	Addr() string
}

// NodeImpl implementation of a node, it has an ID and an Addr.
// attributes are exported only because it is used by yaml decoder. Must not be directly used.
type NodeImpl struct {
	Id      int    `yaml:"id"`
	Address string `yaml:"address"`
}

func (n *NodeImpl) ID() int {
	return n.Id
}

func (n *NodeImpl) Addr() string {
	return n.Address
}

// state represents the state of the actual node Ni. It encapsulates all the data of a node.
type state struct {
	// the minimal weight of the closest outside going edge
	minWeight int
	// fragmentID the id of the fragment the node is part of
	fragmentID int
	// node the actual representation of this node.
	node *NodeImpl
	// toMWOE node used to route the MERGE request to the MWOE of the actual fragment
	nodeToMWOE *NodeImpl
	// parent the parent node in the Spanning Tree.
	parent Node
	// children all its children nodes.
	children []Node // spanning tree neighbours
	// boolChildren
	childrenBool []bool // spanning tree neighbours
	// notChildren all the treated neighbors that refused to be its children.
	notChildren []Node // non spanning tree neighbours
	// neighbors all the neighbors of the node (independent of the SPT protocol).
	neighbors []Neighbour
	// terminated stores the status of the STP algo for this node.
	terminated bool
	// connectionRequestSent an array of bool which has the same size as neighbour array
	// Used by a node to track which neighbours it has sent a connection request to
	// true -> sent connect
	// false -> not sent connect yet
	connectionRequestSentTo []bool
	// connectionRequestSent an array of bool which has the same size as neighbour array
	// Used by a node to track which neighbours it has received a connection request from
	// true -> received connect
	// false -> did not receive connect
	connectionRequestReceivedFrom []bool
	// basic an array of bool which has the same size as neighbour array
	// represent if a neighbour is basic (not a parent or a child)
	// true -> basic
	// false -> not basic
	basic []bool
	// rejected an array of bool which has the same size as neighbour array
	// represent if a neighbour has already sent a REJECT message to a TEST request
	// used to diminished the number of messages passed in the network
	// (if a node already rejected my TEST request, I do not need to send another one)
	// true -> the neighbor has already rejected my TEST
	// false -> the neighbor has not yet  rejected my TEST
	rejected []bool
	// barrierCounter int incremented everytime a node send a TEST OR DOTEST message
	// decremented every time a REPORT / ACCEPT / REJECT message
	// used to synchronize the reports
	barrierCounter int
	// minimalNeighbourTag int the index of the minimal node of an incident node
	// used to send connect request
	minimalNeighbourTag int
	// ackCounter int counter incremented everytime a NEW_FRAGMENT message is sent
	// decremented everytime an ACK message is received
	// used to syncronize the NEW_FRAGMENT phg
	ackCounter int
}

// NewMessageWithWeight creates a Message structure containing the all the information needed inside a Message regarding the actual
// state of this node.
func (s state) NewMessageWithWeight(k messageKind, weight int) Message {
	return Message{
		Kind:       k,
		Source:     s.node,
		FragmentID: s.fragmentID,
		Weight:     weight,
	}
}

// NewMessageWithoutWeight This method  create a new message without the weight
func (s state) NewMessageWithoutWeight(k messageKind) Message {
	return Message{
		Kind:       k,
		Source:     s.node,
		FragmentID: s.fragmentID,
	}
}

// NewMessageWithTag this method creates a new message and put directly the tag given in parameter in it.
func (s state) NewMessageWithTag(k messageKind, tag int) Message {
	return Message{
		Kind:         k,
		Source:       s.node,
		FragmentID:   s.fragmentID,
		NeighbourTag: tag,
	}
}

// initAndParseFileNeighbours initialize the current state for the actual node.
func initAndParseFileNeighbours(filename string) state {
	fullpath, _ := filepath.Abs("./" + filename)
	yamlFile, err := ioutil.ReadFile(fullpath)

	if err != nil {
		panic(err)
	}

	var data yamlConfig

	err = yaml.Unmarshal(yamlFile, &data)
	if err != nil {
		panic(err)
	}
	res := state{
		node: &NodeImpl{
			Id:      data.ID,
			Address: data.Address,
		},
		parent:                        nil,
		neighbors:                     make([]Neighbour, 0, len(data.Neighbours)),
		children:                      nil,
		notChildren:                   nil,
		terminated:                    false,
		nodeToMWOE:                    nil,
		connectionRequestSentTo:       make([]bool, len(data.Neighbours)),
		connectionRequestReceivedFrom: make([]bool, len(data.Neighbours)),
		childrenBool:                  make([]bool, len(data.Neighbours)),
		basic:                         make([]bool, len(data.Neighbours)),
		rejected:                      make([]bool, len(data.Neighbours)),
		barrierCounter:                0,
		minWeight:                     math.MaxInt,
		minimalNeighbourTag:           -1,
		ackCounter:                    0,
	}

	res.neighbors = append(res.neighbors, data.Neighbours...) // neighbours list contains the edge weights of the node
	res.nodeToMWOE = res.node
	res.parent = res.node
	res.fragmentID = res.node.ID() // this creates N independent fragments
	for i := 0; i < len(res.connectionRequestSentTo); i++ {
		res.connectionRequestSentTo[i] = false
		res.connectionRequestReceivedFrom[i] = false
		res.childrenBool[i] = false
		res.basic[i] = true
		res.rejected[i] = false
	}
	return res
}

// nodeLog logs regarding a defined format the message.
func nodeLog(n Node, message string) {
	fmt.Printf("[%d] : %s\n", n.ID(), message)
}

// send the body to the specified address. Generally, it's used via the method sendTo.
func send(neighAddress string, body []byte) {
	checkErr := func(err error) {
		if err != nil {
			log.Fatal(err)
		}
	}
	outConn, err := net.Dial("tcp", neighAddress+PORT)
	checkErr(err)
	_, err = outConn.Write(body)
	checkErr(err)
	checkErr(outConn.Close())
}

// sendTo sends a Message to the destination Node.
func sendTo(dest Node, m Message) {
	// build the message
	body, _ := json.Marshal(m) // TODO : Take care of the error
	go send(dest.Addr(), body)
}

// connectHandler func is called when a CONNECT Message is received or send.
// It is used to handle synchronous links for MERGE. It determines, if we both received and send a CONNECT
// Message from/to the same Node, which one will be the next root of the future merged fragment.
// If it detects that the current node must be the new root of the fragment, it will change its configuration
// to become the new root (parent become child, etc.) and send the NEW_FRAGMENT message.
func connectHandler(currState *state, rcv Message, neighIdx int) {
	// If I did not yet receive an ACCEPT message from my neighbour of the other fragment,
	// I do nothing (this is not an asymmetric link)
	if currState.minimalNeighbourTag == -1 {
		return
	}
	// If I have the biggest ID than the other one, I'll become the new root (if not already), else do nothing.
	if currState.node.ID() > currState.neighbors[currState.minimalNeighbourTag].ID() {

		// Validation that is an asymmetric link.
		if currState.connectionRequestReceivedFrom[neighIdx] && currState.connectionRequestSentTo[neighIdx] {
			nodeLog(
				currState.node, fmt.Sprintf(
					"test %d %d", currState.neighbors[currState.minimalNeighbourTag].ID(),
					currState.neighbors[neighIdx].ID(),
				),
			)
			// The node at the opposite of the link linking the two fragment (which loosed the root election)
			// become my child
			currState.childrenBool[neighIdx] = true
			currState.basic[neighIdx] = false // this is now a direct children, no need to send TEST request to him.
			// I was not the root before, I must become the new one.
			if currState.fragmentID != currState.node.ID() {
				// I set my parent as my child
				// the loop below is just to get the index of my parent into the neighbours list.
				idx := -1
				for i := 0; i < len(currState.neighbors); i++ {
					if currState.neighbors[i].ID() == currState.parent.ID() {
						idx = i
						break
					}
				}
				if idx == -1 {
					nodeLog(currState.node, "IDX IS EQUAL TO MINUS 1 !")
				}
				currState.basic[idx] = false
				currState.childrenBool[idx] = true // parent becomes my child
				// I become the root of this new fragment
				currState.fragmentID = currState.node.ID()
				// I can then modify my parent to myself
				currState.parent = currState.node
			}

			// After setting myself as root if I wasn't before, I can clear my data and send a NEW_FRAGMENT
			// message to everybody!
			currState.connectionRequestReceivedFrom[neighIdx] = false
			currState.connectionRequestSentTo[neighIdx] = false
			nodeLog(currState.node, "Created new fragment and is root")
			// send new fragment message to self
			go sendTo(currState.node, currState.NewMessageWithoutWeight(NEW_FRAGMENT))
		}
	}
}

// server this method represent a node in the network that implements the GHS Minimal Spanning Tree algorithm.
// It can be launched as gorouting with `go server` command.
func server(wginit *sync.WaitGroup, wgdone *sync.WaitGroup, neighboursFilePath string) {
	// Parse file and init the current state.

	currState := initAndParseFileNeighbours(neighboursFilePath)
	nodeLog(currState.node, "waiting for all nodes to be ready")

	ln, err := net.Listen("tcp", currState.node.Address+PORT)
	if err != nil {
		log.Fatal(err)
		return
	}

	// The use of wginit is here to wait that all nodes in the network has successfully started and ready to
	// begin the algorithm.
	// we make a P(wginit) and then the Wait method waits until the wginit == 0.
	wginit.Done()
	wginit.Wait()
	// the barrier counter is used to know when all our children have answered to our request. It is incremented
	// when sending TEST messages and DOTEST
	//  initialized to the number of neighbors because we send a TEST request to every neighbour at initialization.
	currState.barrierCounter = len(currState.neighbors)
	// initialization, send TEST to every neighbours
	for i := 0; i < len(currState.basic); i++ {
		if currState.basic[i] {
			nodeLog(currState.node, fmt.Sprintf("Sending a test request t %d", currState.neighbors[i].Id))
			sendTo(&currState.neighbors[i], currState.NewMessageWithTag(TEST, i))
		}
	}

	// getNodeIndexByID util method to get a node index in the neighbours list regarding its ID
	getNodeIndexByID := func(nodeID int) int {
		for i := 0; i < len(currState.neighbors); i++ {
			if currState.neighbors[i].ID() == nodeID {
				return i
			}
		}
		return -1
	}

	// while we are not terminated. We are terminated when receiving TERMINATE message.
	for !currState.terminated {

		// Accept all incoming connections
		conn, _ := ln.Accept()
		message, _ := bufio.NewReader(conn).ReadBytes('\n')
		// var rcv Message: s.node,ceived byte to message: %s  / err: %s", message, err)
		var rcv Message
		err := json.Unmarshal(message, &rcv)
		if err != nil {
			log.Fatalf("Error occured when trying to convert received byte to message: %s  / err: %s", message, err)
		}
		err = conn.Close()
		if err != nil {
			log.Fatalf("Error occured when closing the connection: %s", err)
		}

		// Switch for type of message we just received
		switch rcv.Kind {
		case NEW_FRAGMENT:
			nodeLog(currState.node, fmt.Sprintf("Received new fragment request from %d", rcv.Source.ID()))
			// we reset the ack counter of the current node to synchronize the NEW_FRAGMENT transmission.
			// this counter is incremented each time we propagate the NEW_FRAGMENT and decremented each time we receive
			// an ACK. It forces the node to wait on all children before sending the ACK to parent.
			currState.ackCounter = 0
			// my root becomes fragment id
			currState.fragmentID = rcv.FragmentID
			// reset the minWeight to maxint for every node of the new fragment
			currState.minWeight = math.MaxInt

			// a node different from the root received the message
			if currState.node.ID() != rcv.FragmentID {
				// I must put my parent in my children if I have one (There is a new Root in the fragment)
				if currState.node.ID() != currState.parent.ID() {
					idx := getNodeIndexByID(currState.parent.ID())
					currState.basic[idx] = false
					currState.childrenBool[idx] = true // parent becomes my child
				}
				currState.parent = rcv.Source
				// if a former children of mine sent me a new fragment i need to remove it from my children list
				idx := getNodeIndexByID(rcv.Source.ID())
				currState.basic[idx] = false
				currState.childrenBool[idx] = false
				currState.parent = &currState.neighbors[idx]
			}

			// Check for asymmetric connect, if received an asymmetric connect from a node, I can add him directly
			// in my fragment.
			for i := 0; i < len(currState.connectionRequestReceivedFrom); i++ {
				// DID THIS NODE RECEIVE CONNECT REQUEST from another node that the new root
				if currState.connectionRequestReceivedFrom[i] && currState.neighbors[i].ID() != currState.parent.ID() {
					// if yes the node that sent me a connect becomes my child
					// and is not a basic node
					currState.connectionRequestReceivedFrom[i] = false
					currState.childrenBool[i] = true
					currState.basic[i] = false
				}
			}

			// Propagation of the NEW_FRAGMENT to all children (incrementation of the ackCounter at each send).
			hasChildren := false
			for i := 0; i < len(currState.childrenBool); i++ {
				if currState.childrenBool[i] {
					hasChildren = true
					currState.ackCounter++
					sendTo(&currState.neighbors[i], currState.NewMessageWithoutWeight(NEW_FRAGMENT))
				}
			}

			// if I am a leaf, I only send to my parent an ACK request
			if !hasChildren {
				// report ack to parent
				sendTo(currState.parent, currState.NewMessageWithoutWeight(ACK))
			}
		case CONNECT:
			// I received a message from someone
			nodeLog(currState.node, fmt.Sprintf("RCV CONNECT FROM %d", rcv.Source.ID()))
			// I received a CONNECT message from a node and need to find its index in my array
			neighIdx := getNodeIndexByID(rcv.Source.ID())
			// Write down that I received a connect request from this node and call the connectHandler.
			currState.connectionRequestReceivedFrom[neighIdx] = true
			connectHandler(&currState, rcv, neighIdx)
		case MERGE:
			nodeLog(currState.node, "Received MERGE ")
			if currState.nodeToMWOE.ID() == currState.node.ID() { // I am the incident node
				// I then send a connect request and call my connectHandler
				nodeLog(
					currState.node,
					fmt.Sprintf("SENDING CONNECT TO %d", currState.neighbors[currState.minimalNeighbourTag].ID()),
				)
				// Tag that I sent a connect request to my minimal neighbour
				currState.connectionRequestSentTo[currState.minimalNeighbourTag] = true
				// Send the CONNECT message
				go sendTo(
					&currState.neighbors[currState.minimalNeighbourTag],
					currState.NewMessageWithTag(CONNECT, currState.minimalNeighbourTag),
				)
				// Tells the handler that I sent a CONNECT.
				connectHandler(&currState, rcv, currState.minimalNeighbourTag)
			} else { // I am not the incident node, so I forward the merge to the incident node
				go sendTo(currState.nodeToMWOE, currState.NewMessageWithoutWeight(MERGE))
			}
		case TEST:
			nodeLog(currState.node, fmt.Sprintf("Received a test request from %d", rcv.Source.ID()))
			// Am I part of the same fragment as the sender?
			if rcv.FragmentID != currState.fragmentID { // NO
				// I am not in the same fragment as the sender, I accept its TEST.
				go sendTo(rcv.Source, currState.NewMessageWithTag(ACCEPT, rcv.NeighbourTag))
			} else { // YES
				// I am in the same fragment as the sender, I reject its TEST.
				go sendTo(rcv.Source, currState.NewMessageWithTag(REJECT, rcv.NeighbourTag))
			}
		case ACCEPT:
			nodeLog(currState.node, fmt.Sprintf("Received an accept request from %d", rcv.Source.ID()))
			// I got an ACCEPT response from a TEST message I sent.
			// Retrieve the neighbour and weight that sent me the accept request
			currState.rejected[rcv.NeighbourTag] = false
			// If the connection between the sender and me has less wait that I already find,
			// this is (for now) the minimal link to another fragment. I keep this weight by storing into
			// currState.minWeight and consider myself as my "nodeToMWOE" as I am connected to another fragment.
			if currState.neighbors[rcv.NeighbourTag].EdgeWeight < currState.minWeight {
				currState.minWeight = currState.neighbors[rcv.NeighbourTag].EdgeWeight
				currState.nodeToMWOE = currState.node
				// the minimalNeighbourTag is here to register the index (regarding my neighbour list)
				// of the neighbour that is at the opposite of the minimal link.
				currState.minimalNeighbourTag = rcv.NeighbourTag
			}
			// decrement the barrierCounter used to wait on the response of all TEST messages I sent.
			currState.barrierCounter-- // local var thread safe
		case REJECT:
			nodeLog(currState.node, "Received REJECT ")
			// I received a REJECT answer to my TEST message, so decrement the barrierCounter
			//  and set the neighbour as "Rejected".
			currState.barrierCounter--
			currState.rejected[rcv.NeighbourTag] = true
		case REPORT:
			nodeLog(currState.node, "Received REPORT ")
			// I received a REPORT in response to my DOTEST request, I can decrease the barrierCounter
			currState.barrierCounter--
			// If the REPORT I received have better weight that I found (this is the way to go),
			// I set my weight to the weight of the report and save the nodeToMWOE as the sender of the REQUEST.
			// Now, my "nodeToMWOE" is the next hope to the MWOE of the fragment.
			if rcv.Weight < currState.minWeight { // YES
				currState.nodeToMWOE = rcv.Source
				currState.minWeight = rcv.Weight
			}
		case ACK:
			// I received an ACK, it means that one of my children finished the propagation of the NEW_FRAGMENT message.
			// so I can decrement the ackCounter used to wait on all ACK from children.
			currState.ackCounter--
			// if we received all ACKS from our children...
			if currState.ackCounter == 0 {
				// ...and we are not the root, propagate the ACK to the parent.
				if currState.fragmentID != currState.node.ID() {
					// report ACK up if not root
					sendTo(currState.parent, currState.NewMessageWithoutWeight(ACK))
				} else { // ...and we are the root
					// it means that we just finished a phase as I am sure that all the node in my fragment
					// have received the NEW_FRAGMENT.
					// Now, we can send a DOTEST to all the node inside the Fragment.
					// Here, we send DOTEST message to ourselves but the implementation of the DOTEST case
					// implies to propagate it to all our children, so yes, as root, we propagate it to everybody.
					sendTo(currState.node, currState.NewMessageWithoutWeight(DOTEST))
				}
			}
		case DOTEST:
			// We received a DOTEST, it means that we are beginning a new phase.
			// we reset the barrierCounter
			currState.barrierCounter = 0
			// we will iterate over our neighbour
			for i := 0; i < len(currState.basic); i++ {
				// if the neighbour is basic and has not sent a REJECT message
				// send TEST message to it and increment the barrier
				if currState.basic[i] && !currState.rejected[i] {
					// The barrierCounter is incremented for waiting on all response of TEST messages
					currState.barrierCounter++
					// send test
					sendTo(&currState.neighbors[i], currState.NewMessageWithTag(TEST, i))
				} else if currState.childrenBool[i] {
					// else if the neighbour is my child
					// propagate the DOTEST message, increment the barrierCounter to wait on all reports
					currState.barrierCounter++
					sendTo(&currState.neighbors[i], currState.NewMessageWithoutWeight(DOTEST))
				}
			}
		case TERMINATED: // stop immediate
			nodeLog(currState.node, fmt.Sprintf("terminated with parent %d", currState.parent.ID()))
			nodeLog(currState.node, fmt.Sprintf("terminated with children %t ", currState.childrenBool))
			nodeLog(currState.node, fmt.Sprintf("terminated with neighbours %s \n", currState.neighbors))
			// set the terminated variable to true, we will then exit the main loop of the server.
			currState.terminated = true
		}

		// At the end, we check the barrier counter.
		// If the barrier counter == 0, it means that I received a REPORT message from all my child
		// and, I received a response to all TEST message I sent.
		if currState.barrierCounter == 0 { // received everything we can report up / merge down / terminate
			nodeLog(currState.node, "passed sync barrier")
			// set the barrierCounter to invalid value just in case, but it will be reset to 0 when we receive
			// the next DOTEST in the next phase (level).
			currState.barrierCounter = -1
			// If I am root, I received all the reports and, I am able now to choose between sending a MERGE
			// request or stopping the algorithm.
			if currState.fragmentID == currState.node.ID() {
				log.Printf("minweight %d\n", currState.minWeight)
				// If the minimal weight I received from all the reports are MaxInt, it means that the algo is finished.
				if currState.minWeight == math.MaxInt { // all nodes are inbound we are done with the alg
					// I terminate myself
					// the send of "TERMINATED" messages will be done when exiting the main loop.
					currState.terminated = true
					nodeLog(currState.node, "sync TERMINATED")
					log.Printf(
						"Node %d is the final root with following children %t \n", currState.node.ID(),
						currState.childrenBool,
					)
					log.Printf(
						"Node %d is the final root with neighbours %s \n", currState.node.ID(), currState.neighbors,
					)
				} else {
					// (as root) If the minimal weight I received != MaxInt,
					// it means that there still fragments to MERGE. Sen a MERGE to the MWOE of the fragment.
					go sendTo(currState.nodeToMWOE, currState.NewMessageWithoutWeight(MERGE))
					nodeLog(currState.node, fmt.Sprintf("sync merge to %d ", currState.nodeToMWOE.ID()))
				}
			} else { // I received every REPORT and TEST answer from my children, and I am not the root,
				//       so I can send my REPORT to my parent.
				go sendTo(currState.parent, currState.NewMessageWithWeight(REPORT, currState.minWeight))
				nodeLog(currState.node, "sync REPORT")
			}
		}
	}
	// We are out of the main loop, it means that we terminated. We use the spanning tree created by the algo
	// to broadcast the TERMINATED message to everybody by sending TERMINATED to our children.
	for i := 0; i < len(currState.childrenBool); i++ {
		if currState.childrenBool[i] {
			sendTo(&currState.neighbors[i], currState.NewMessageWithoutWeight(TERMINATED))
		}
	}
	// Decrement the "semaphore" wgdone when we are finished. This is a P(wgdone). The main thread
	// will wait until the wgdone == 0 and then stop himself.
	wgdone.Done()
}

func main() {
	// WaitGroup are like semaphores, but with unlimited counters.
	// we use them to synchronize things in golang. The .Add() method add a value to the counter, the ".Done()"
	// decrements this value by one and the ".Wait()" wait until the counter == 0.
	// Here we use initwq to synchronize the nodes at the start, all nodes make a Done and then Wait
	// and the donewg is used for the main thread to know when the algorithm is finished, all server call Done
	// when they are terminated.
	var initwg sync.WaitGroup
	var donewg sync.WaitGroup
	initwg.Add(11) // initialize to 11 because 11 servers
	donewg.Add(11) // initialize to 11 because 11 servers

	// START ALL NODES!
	go server(&initwg, &donewg, "node-2.yaml")
	go server(&initwg, &donewg, "node-3.yaml")
	go server(&initwg, &donewg, "node-4.yaml")
	go server(&initwg, &donewg, "node-5.yaml")
	go server(&initwg, &donewg, "node-7.yaml")
	go server(&initwg, &donewg, "node-1.yaml")
	go server(&initwg, &donewg, "node-6.yaml")
	go server(&initwg, &donewg, "node-8.yaml")
	go server(&initwg, &donewg, "node-9.yaml")
	go server(&initwg, &donewg, "node-10.yaml")
	go server(&initwg, &donewg, "node-11.yaml")
	// Wait until every server call Done when they terminate.
	donewg.Wait()
	fmt.Printf("DONE")
}
